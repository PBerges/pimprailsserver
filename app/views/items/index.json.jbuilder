json.array!(@items) do |item|
  json.extract! item, :id, :title, :price,:owners
  json.url item_url(item, format: :json)
end
