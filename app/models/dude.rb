class Dude < ActiveRecord::Base
  has_many :items_dudes
  has_many :items, through: :items_dude
end
