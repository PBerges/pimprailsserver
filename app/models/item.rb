class Item < ActiveRecord::Base
  has_many :items_dudes
  has_many :owners, through: :items_dudes
end
