class ItemsDudesController < ApplicationController
  before_action :set_items_dude, only: [:show, :edit, :update, :destroy]

  # GET /items_dudes
  # GET /items_dudes.json
  def index
    @items_dudes = ItemsDude.all
  end

  # GET /items_dudes/1
  # GET /items_dudes/1.json
  def show
  end

  # GET /items_dudes/new
  def new
    @items_dude = ItemsDude.new
  end

  # GET /items_dudes/1/edit
  def edit
  end

  # POST /items_dudes
  # POST /items_dudes.json
  def create
    @items_dude = ItemsDude.new(items_dude_params)

    respond_to do |format|
      if @items_dude.save
        format.html { redirect_to @items_dude, notice: 'Items dude was successfully created.' }
        format.json { render action: 'show', status: :created, location: @items_dude }
      else
        format.html { render action: 'new' }
        format.json { render json: @items_dude.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items_dudes/1
  # PATCH/PUT /items_dudes/1.json
  def update
    respond_to do |format|
      if @items_dude.update(items_dude_params)
        format.html { redirect_to @items_dude, notice: 'Items dude was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @items_dude.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items_dudes/1
  # DELETE /items_dudes/1.json
  def destroy
    @items_dude.destroy
    respond_to do |format|
      format.html { redirect_to items_dudes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_items_dude
      @items_dude = ItemsDude.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def items_dude_params
      params.require(:items_dude).permit(:id_items, :id_dudes)
    end
end
