class CreateItemsDudes < ActiveRecord::Migration
  def change
    create_table :items_dudes do |t|
      t.integer :id_items
      t.integer :id_dudes

      t.timestamps
    end
  end
end
