class RenamenColumnsOfItemsDudes < ActiveRecord::Migration
  def change
    rename_column :items_dudes, :id_items, :item_id
    rename_column :items_dudes, :id_dudes, :owner_id
  end
end
