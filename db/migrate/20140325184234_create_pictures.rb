class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :pic, :binary

      t.timestamps
    end
  end
end
