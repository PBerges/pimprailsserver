require 'test_helper'

class ItemsDudesControllerTest < ActionController::TestCase
  setup do
    @items_dude = items_dudes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:items_dudes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create items_dude" do
    assert_difference('ItemsDude.count') do
      post :create, items_dude: { id_dudes: @items_dude.id_dudes, id_items: @items_dude.id_items }
    end

    assert_redirected_to items_dude_path(assigns(:items_dude))
  end

  test "should show items_dude" do
    get :show, id: @items_dude
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @items_dude
    assert_response :success
  end

  test "should update items_dude" do
    patch :update, id: @items_dude, items_dude: { id_dudes: @items_dude.id_dudes, id_items: @items_dude.id_items }
    assert_redirected_to items_dude_path(assigns(:items_dude))
  end

  test "should destroy items_dude" do
    assert_difference('ItemsDude.count', -1) do
      delete :destroy, id: @items_dude
    end

    assert_redirected_to items_dudes_path
  end
end
